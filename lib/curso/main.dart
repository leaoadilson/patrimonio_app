

import 'package:flutter/material.dart';
import './resultado.dart';

import './quiz.dart';

// void main(){
//   runApp(MyApp());
// }

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final _questoes = const [
    {
      'pergunta': 'Qual sua cor favorita?',
      'respostas': [
        {'texto': 'Vermelho', 'ponto': 10},
        {'texto': 'Verde', 'ponto': 8},
        {'texto': 'Preto', 'ponto': 5},
        {'texto': 'Azul', 'ponto': 5},
      ],
    },
    {
      'pergunta': 'Qual seu animal favorito?',
      'respostas': [
        {'texto': 'Gato', 'ponto': 8},
        {'texto': 'Cachorro', 'ponto': 12},
        {'texto': 'Coelho', 'ponto': 3},
      ],
    },
    {
      'pergunta': 'Qual seu músico favorito?',
      'respostas': [
        {'texto': 'Lorde', 'ponto': 2},
        {'texto': 'Pericles', 'ponto': 12},
        {'texto': 'Xuxa', 'ponto': 6},
      ]
    },
  ];

  var _questoaoIndex = 0;
  var _pontoTotal = 0;

  void _resetarQuiz() {
    setState(() {
      _questoaoIndex = 0;
      _pontoTotal = 0;
    });
  }

  void _responderQuestao(int pontos) {
    _pontoTotal += pontos;
    setState(() {
      _questoaoIndex = _questoaoIndex + 1;
    });
    if (_questoaoIndex < _questoes.length) {
      print('ha mais _questoes');
    } else {
      print('nao ha mais _questoes');
    }
    print(_questoaoIndex);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App de Teste',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Teste'),
        ),
        body: _questoaoIndex < _questoes.length
            ? Quiz(
                indexQuentao: _questoaoIndex,
                responder: _responderQuestao,
                questoes: _questoes,
              )
            : Resultado(
               _pontoTotal,
               _resetarQuiz,
              ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
