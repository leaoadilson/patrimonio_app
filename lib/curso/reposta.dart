import 'package:flutter/material.dart';

class Resposta extends StatelessWidget {
  
  final Function funcaoResposta;
  final String questaoTexto;
  
  const Resposta(this.funcaoResposta, this.questaoTexto);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal:50),
      child: RaisedButton(
        color: Colors.yellow[300],
        child: Text(questaoTexto,style: TextStyle(fontSize:16),),
        onPressed: funcaoResposta,
      ),
    );
  }
}
