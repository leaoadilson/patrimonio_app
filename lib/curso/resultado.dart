import 'package:flutter/material.dart';

class Resultado extends StatelessWidget {
  const Resultado(this.pontoFinal, this.resetartQuiz);

  final int pontoFinal;
  final Function resetartQuiz;

  String get fraseFinal {
    print(pontoFinal);
    var frase;
    if (pontoFinal <= 12) {
      frase = 'Você é demais!';
    } else if (pontoFinal <= 27) {
      frase = 'Te falta ódio';
    } else {
      frase = 'Você é mal';
    }
    return frase;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            fraseFinal,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 36,
            ),
            textAlign: TextAlign.center,
          ),
          FlatButton(
            child: Text('Reiniciar'),
            onPressed: resetartQuiz,
            textColor: Colors.black,
            color: Colors.yellow,
          ),
        ],
      ),
    );
  }
}
