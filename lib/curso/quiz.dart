import 'package:flutter/material.dart';
import './question.dart';
import './reposta.dart';

class Quiz extends StatelessWidget {
  final List<Map<String, Object>> questoes;
  final int indexQuentao;
  final Function responder;

  const Quiz({Key key, @required this.questoes, @required this.responder, @required this.indexQuentao}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Question(
                    questoes[indexQuentao]['pergunta'],
                  ),
                  ...(questoes[indexQuentao]['respostas'] as List<Map<String, Object>>)
                      .map((questao) {
                    return Resposta(() => responder(questao['ponto']), questao['texto']);
                  }).toList()
                ],
              ),
    );
  }
}