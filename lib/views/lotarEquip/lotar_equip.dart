import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class LotarEquip extends StatefulWidget {
  LotarEquip({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _LotarEquipState createState() => _LotarEquipState();
}

class _LotarEquipState extends State<LotarEquip> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(25),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              MyCustomForm(),
            ]),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  final databaseReference = FirebaseDatabase.instance.reference();

  List<Map> _listaEquipamentos = [];

  String _equipLotar;

  String _setorLotar;

  @override
  void initState() {
    databaseReference.child('equipamentos').once().then((DataSnapshot data) {
      if (data != null) {
        var lista = new Map<String, dynamic>.from(data.value);
        lista.forEach((k, v) {
          setState(() {
            _listaEquipamentos.add(v);
          });
        });
      }
    });
    super.initState();
  }

  final listaSetores = const [
    'TI',
    'Juridico',
    'Administrativo',
    'Atendimento'
  ];

  Map _lotacao = {
    "equipamento": null,
    "setor": null,
  };

  void _salvarLotacao() {
    var _equipSalvar;
    var form = _formKey.currentState;
    if (form.validate()) {
      try {
        form.save();
        _listaEquipamentos.forEach((value) {
          if (value["patrimonio"] == _equipLotar) {
            _equipSalvar = value;
          }
        });
        setState(() {
          _lotacao["setor"] = _setorLotar;
          _lotacao["equipamento"] = _equipSalvar;
        });
        databaseReference
            .child('lotacoes')
            .push()
            .set(_lotacao)
            .catchError((onError) {
          throw Error;
        });
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text('Lotação salva')));
        _formKey.currentState.reset();
        Future.delayed(Duration(seconds: 2), () => Navigator.pop(context));
      } catch (e) {
        print(e);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          DropdownButtonFormField<String>(
            value: _equipLotar,
            hint: Text("Selecione o Equipamento"),
            validator: (value) =>
                value == null ? 'Selecione um Equipamento' : null,
            isExpanded: true,
            items: _listaEquipamentos.map((Map value) {
              return new DropdownMenuItem<String>(
                value: value["patrimonio"],
                child: new Text(value["patrimonio"] + " - " + value["tipo"]),
              );
            }).toList(),
            onChanged: (String newValue) {
              setState(() {
                _equipLotar = newValue;
              });
            },
          ),
          SizedBox(
            height: 20,
          ),
          DropdownButtonFormField<String>(
            value: _setorLotar,
            validator: (value) => value == null ? 'Selecione um setor' : null,
            hint: Text("Selecione o setor"),
            isExpanded: true,
            items: listaSetores.map((String value) {
              return new DropdownMenuItem<String>(
                value: value,
                child: new Text(value),
              );
            }).toList(),
            onChanged: (String newValue) {
              setState(() {
                _setorLotar = newValue;
              });
            },
          ),
          SizedBox(
            height: 20,
          ),
          RaisedButton(
            color: Colors.orange[500],
            onPressed: _salvarLotacao,
            child: Text('Salvar',style: TextStyle(color: Colors.white),),
          ),
        ],
      ),
    );
  }
}
