import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/services.dart';
import 'package:patrimonioapp/models/equip.dart';

class CadastrarEquip extends StatefulWidget {
  CadastrarEquip({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _CadastrarEquipState createState() => _CadastrarEquipState();
}

class _CadastrarEquipState extends State<CadastrarEquip> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
        ),
        //leading: IconButton(icon: Icon(Icons.menu), onPressed: null),
      ),
      body: SingleChildScrollView(
        child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: double.infinity,
                  margin: EdgeInsets.all(25),
                  child: MyCustomForm()),
            ]),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  final databaseReference = FirebaseDatabase.instance.reference();
  EquipData _data = new EquipData();

  void _salvarEquip() {
    var _dataSalvar = {
      "tipo": _data.tipo,
      "ano": _data.ano,
      "marca": _data.marca,
      "patrimonio": _data.patrimonio
    };
    databaseReference
        .child("equipamentos")
        .child(_dataSalvar["patrimonio"])
        .once()
        .then((DataSnapshot data) {
      if (data.value == null) {
        print('não existe');

        databaseReference
            .child('equipamentos')
            .child(_dataSalvar["patrimonio"])
            .set(_dataSalvar)
            .catchError((onError) {
          throw Error;
        });
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text('Equipamento salvo')));
        _formKey.currentState.reset();
        Future.delayed(Duration(seconds: 2), () => Navigator.pop(context));
      } else {
        print('existe');
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Já existe equipamento com esse patrimônio')));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            inputFormatters: <TextInputFormatter>[
              WhitelistingTextInputFormatter.digitsOnly
            ],
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              labelText: 'Patrimonio',
            ),
            validator: (value) {
              if (value.isEmpty) {
                return 'O campo não pode ser deixado em branco';
              }
              return null;
            },
            onSaved: (String value) {
              this._data.patrimonio = value;
            },
          ),
          TextFormField(
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
              labelText: 'Tipo de Equipamento',
            ),
            validator: (value) {
              if (value.isEmpty) {
                return 'O campo não pode ser deixado em branco';
              }
              return null;
            },
            onSaved: (String value) {
              this._data.tipo = value;
            },
            onFieldSubmitted: (v) {
              FocusScope.of(context).nextFocus();
            },
          ),
          TextFormField(
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
              labelText: 'Marca',
            ),
            validator: (value) {
              if (value.isEmpty) {
                return 'O campo não pode ser deixado em branco';
              }
              return null;
            },
            onSaved: (String value) {
              this._data.marca = value;
            },
            onFieldSubmitted: (v) {
              FocusScope.of(context).nextFocus();
            },
          ),
          TextFormField(
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              WhitelistingTextInputFormatter.digitsOnly,
              new LengthLimitingTextInputFormatter(4),
            ],
            decoration: InputDecoration(
              labelText: 'Ano de Compra',
            ),
            validator: (value) {
              if (value.isEmpty) {
                return 'O campo não pode ser deixado em branco';
              }
              return null;
            },
            onSaved: (String value) {
              this._data.ano = value;
            },
            onFieldSubmitted: (v) {
              FocusScope.of(context).nextFocus();
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15.0),
            child: RaisedButton(
              color: Colors.orange[500],
              onPressed: () {
                // Validate returns true if the form is valid, or false
                // otherwise.
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  try {
                    _salvarEquip();
                  } catch (e) {
                    print(e);
                  }
                }
              },
              child: Text(
                'Salvar',
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
