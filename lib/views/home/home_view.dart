import 'package:flutter/material.dart';
import 'package:patrimonioapp/views/cadastrarEquip/cadastrar_equip.dart';
import 'package:patrimonioapp/views/lotarEquip/lotar_equip.dart';
import 'package:flutter_offline/flutter_offline.dart';

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Builder(
        builder: (BuildContext context) {
          return OfflineBuilder(
            connectivityBuilder: (BuildContext context,
                ConnectivityResult connectivity, Widget child) {
              final bool connected = connectivity != ConnectivityResult.none;
              return Stack(
                fit: StackFit.expand,
                children: [
                  child,
                  // Positioned(
                  //   left: 0.0,
                  //   right: 0.0,
                  //   height: 32.0,
                  //   child: AnimatedContainer(
                  //     duration: const Duration(milliseconds: 300),
                  //     color: connected ? Color(0xFF00EE44) : Color(0xFFEE4400),
                  //     child: connected
                  //         ? Row(
                  //             mainAxisAlignment: MainAxisAlignment.center,
                  //             children: <Widget>[
                  //               Text(
                  //                 "ONLINE",
                  //                 style: TextStyle(color: Colors.white),
                  //               ),
                  //             ],
                  //           )
                  //         : Row(
                  //             mainAxisAlignment: MainAxisAlignment.center,
                  //             children: <Widget>[
                  //               Text(
                  //                 "OFFLINE",
                  //                 style: TextStyle(color: Colors.white),
                  //               ),
                  //               SizedBox(
                  //                 width: 8.0,
                  //               ),
                  //               SizedBox(
                  //                 width: 12.0,
                  //                 height: 12.0,
                  //                 child: CircularProgressIndicator(
                  //                   strokeWidth: 2.0,
                  //                   valueColor: AlwaysStoppedAnimation<Color>(
                  //                       Colors.white),
                  //                 ),
                  //               ),
                  //             ],
                  //           ),
                  //   ),
                  // ),
                  connected
                      ? Container(
                          width: double.infinity,
                          child: Padding(
                            padding: EdgeInsets.all(25),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => CadastrarEquip(
                                            title: 'Cadastrar Equipamento'),
                                      ),
                                    );
                                  },
                                  child: Card(
                                    color: Colors.grey[300],
                                    child: Container(
                                      width: 180,
                                      height: 180,
                                      child: Padding(
                                        padding: EdgeInsets.all(15),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Icon(
                                              Icons.add_to_photos,
                                              size: 80,
                                              color: Colors.deepOrange,
                                            ),
                                            Spacer(),
                                            Text(
                                              'Cadastrar Equipamento',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .title,
                                              textAlign: TextAlign.center,
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => LotarEquip(
                                            title: 'Lotar Equipamento'),
                                      ),
                                    );
                                  },
                                  child: Card(
                                    color: Colors.grey[300],
                                    child: Container(
                                      width: 180,
                                      height: 180,
                                      child: Padding(
                                        padding: EdgeInsets.all(15),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Icon(
                                              Icons.book,
                                              size: 80,
                                              color: Colors.deepOrange,
                                            ),
                                            Spacer(),
                                            Text(
                                              'Lotar Equipamento',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .title,
                                              textAlign: TextAlign.center,
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      : Center(
                          child: Text("SEM CONEXÃO"),
                        ),
                ],
              );
            },
            child:Center(),
          );
        },
      ),
    );
  }
}
